
require('../lib/mongo');
let msg = require('../lib/message');
async function update() {

    let index = 0;

    await Message.remove({text: {$regex: /^addPublicGroup:\ /}})
    // await ChatMap.remove({})

    while (true) {

        let txns = await Transaction.find({"data.message":{ $regex: /^addPublicGroup:\ / },$where: "this.data.message.length > 4"}).skip(index * 1000).limit(1000);
        if (txns.length == 0) break;
        console.log(index)
        for (let tx of txns) {

            if (tx.type != 254 && tx.data &&tx.data.message && tx.data.message.length > 3){

                await msg.processMessage(tx);
            }

        }
        index++;
    }

    process.exit(0);
}




update();
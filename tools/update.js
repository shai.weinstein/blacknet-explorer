require('../lib/mongo');
const API = require('../lib/api');

async function updateWithModel(model) {

	let index = 0;
	let estimate = await model.estimatedDocumentCount({});
	while(index<estimate){
		let account = await model.find({}).skip(index).limit(1);
		let outleases, inLeases, canceloutleases, cancelinLeases;
		let realBalance = 0, outLeasesBalance = 0, inLeasesBalance = 0;

		outleases = await Transaction.find({ type: 2, from: account[0].address });
		inLeases = await Transaction.find({ type: 2, to: account[0].address });

		canceloutleases = await Transaction.find({ type: 3, from: account[0].address });
		cancelinLeases = await Transaction.find({ type: 3, to: account[0].address });

		if (outleases.length > 0) {
			for (let tx of outleases) {
				outLeasesBalance += tx.data.amount;
			}

			for (let tx of canceloutleases) {
				outLeasesBalance -= tx.data.amount;
			}}

		if (inLeases.length > 0) {
			for (let tx of inLeases) {
				inLeasesBalance += tx.data.amount;
			}

			for (let tx of cancelinLeases) {
				inLeasesBalance -= tx.data.amount;
			}}

		outLeasesBalance = outLeasesBalance < 0 ? 0 : outLeasesBalance;
		inLeasesBalance = inLeasesBalance < 0 ? 0 : inLeasesBalance;
		realBalance =  await account[0].confirmedBalance + outLeasesBalance;
		try {
			await model.findOneAndUpdate({address: account[0].address }, {realBalance: realBalance});
		} catch(err) {
			console.log(err);
		}
		await model.findOneAndUpdate({address: account[0].address }, {outLeasesBalance: outLeasesBalance});
		await model.findOneAndUpdate({address: account[0].address }, {inLeasesBalance: inLeasesBalance});

		console.log(index," ",account[0].address);
		index++;
	}
}

async function update() {
	await updateWithModel(Account);
	process.exit(0);
}

update();

var Utils = {
    verifyAccount: (account)=>{
        if(/^blacknet[a-z0-9]{59}$/i.test(account)){
            return true
        }
        return false
    },
    verifyHash: (hash)=>{
        if(/(\w){64}/i.test(String(hash))){
            return true
        }
        return false
    },
    verifySign: (sign)=>{
        if(Object.prototype.toString.call(sign) === "[object String]" && sign.length === 128){
            return true
        }
        return false
    }, 
    verifyAlias: (alias)=>{
        if(Object.prototype.toString.call(alias) === "[object String]"){
            return true
        }
        return false
    }
}

Blacknet.controller('aliasController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    if(window.location && window.location.pathname){
        var ps = window.location.pathname.split("/");
        if(ps[1]){
            $scope.account = ps[2]
        }
    }

    $scope.res_msg = ""

    $scope.claimClick = function claimClick() {
        
        
        if(!Utils.verifyAlias($scope.display_name)){
            document.querySelector("#display_name").focus()
            return 
        }
        if(!Utils.verifySign($scope.signature)){
            document.querySelector("#signature").focus()
            return 
        }
        $scope.res_msg = "";
        $http.get(`/api/alias?account=${$scope.account}&signature=${$scope.signature}&display_name=${$scope.display_name}`).then(function (res) {
            if(res.status == 200 && res.data.status){
                $timeout(function () {
                    window.location.href="/account/"+ $scope.account
                }, 1000 * 1);
            }else{
                $scope.res_msg = res.data.message
            }
        })
    }

}]);

Blacknet.controller('recent_blocks', ['$scope', '$http', 'USER','$timeout', function ($scope, $http,USER, $timeout) {

    $scope.blocks = [];
    
    function update(){
        $http.get('/api/recent_blocks').then(function (res) {
            $scope.blocks = res.data;
        });
        $timeout(update, 1000 * 30);
    }
    update();

}]);

Blacknet.controller('recent_transactions', ['$scope', '$http', '$timeout',function ($scope, $http, $timeout) {

    $scope.transactions = [];
    $scope.txpool = [];
    function update(){
        $http.get('/api/recent_transactions').then(function (res) {
            $scope.transactions = res.data.rtxns;
            $scope.txpool = res.data.txns;
        })
        $timeout(update, 1000 * 30);
    }
    console.log('users')
    update();
}]);

Blacknet.controller('top_accounts', ['$scope', '$http', function ($scope, $http) {
    $scope.top_accounts = [];
    $http.get('/api/top_accounts' + location.search).then(function (res) {
        $scope.top_accounts = res.data;
    });
    $http.get('/api/top_accounts_chardata' + location.search).then(function (res) {
        var params = {
            title: res.data.params.title,
            name: res.data.params.name
        }
        var chart = {
            backgroundColor: "#3B3B3B",
            plotBackgroundColor: "#3B3B3B",
            plotBorderWidth: null,
            plotShadow: false
        };
        var title = {
            text: params.title,
            style: { "color": "#e8ba3f", "fontSize": "18px" }
        };      
        var tooltip = {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        };
        var plotOptions = {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || '#adadad'
                    }
                }
            }
        };
        var series= [{
            type: 'pie',
            name: params.name,
            data: res.data.chart}]; 
        var json = {
            credits:{
                enabled: false
            }
        };   
        json.chart = chart; 
        json.title = title;     
        json.tooltip = tooltip;  
        json.series = series;
        json.plotOptions = plotOptions;
        $('.chart').highcharts(json);  
    });
}]);

Blacknet.controller('recent_messages', ['$scope', '$http', function ($scope, $http) {
    
    $scope.messages = [];
    function update(){
        $http.get('/api/recent_messages').then(function (res) {
            let msgs = res.data.map(function(msg){

                let text = '', action = msg.text.split(':')[0], data = {};
                if(action == 'profile' && false){
                    text += `${msg.from}` + ' set ';

                    try {
                        let profile = JSON.parse(Base64.decode(msg.text.split(': ')[1]));

                        for(let key in profile){
                            text += ` ${key}: "${profile[key]}"`
                        }

                    } catch (error) {
                        
                    }
                    msg.text = text;
                }
                if(action == 'post'){
                    text += ' new post:  ';
                    try {
                        data = JSON.parse(Base64.decode(msg.text.split(': ')[1]));
                    } catch (error) {
                        
                    }
                    msg.text = text;
                }

                if(action == 'chat' && false){
                    text = ' private message: *******************************';
                    msg.text = text;
                }
                msg.data = data;
                return msg;
            });

            $scope.messages = msgs.filter(function(msg){
                return msg.data.title;
            })
        })
    }
    update();
    
}]);

Blacknet.controller('transaction', ['$scope', '$http', function ($scope, $http) {
    
}]);

Blacknet.controller('forkController', ['$scope', '$http', function ($scope, $http) {
    
    let url = '/api' + location.pathname;

    function fetch(){

        $http.get(url).then(function (res) {
            $scope.forkV2 = res.data.forkV2;
            let percent = $scope.forkV2 * 100 / 1351;
            $scope.percent = percent > 100 ? '100%' : percent.toFixed(2) + '%';
        });
    }
    $scope.forkV2 = 1351;
    $scope.percent = '100%';

}]);



Blacknet.controller('blockController', ['$scope', '$http', function ($scope, $http) {
    $scope.block = {};
    $scope.isloaded = false;
    let url = '/api/v2' + location.pathname;

    if(location.pathname.indexOf('/block/') === -1){
        url = '/api/v2/block' + location.pathname;
    }

    let height = document.querySelector('#block_detail').getAttribute('data-height');
   
    if(height == 0){
        url = '/api/block_genesis';
    }
    
    $http.get(url).then(function (res) {
        $scope.isloaded = true;
        // res.data.transactions = res.data.transactions.map(function(tx){
        //     if(typeof tx.data == 'string') tx.data = JSON.parse(tx.data);
        //     return tx;
        // });
        $scope.transactions = res.data;
    });
}]);

Blacknet.controller('account', ['$scope', '$http', function ($scope, $http) {
    $scope.account = {
        balance: 0,
        confirmedBalance: 0,
        stakingBalance: 0
    };
    $scope.txns = [];
    $scope.isGt10000 = false;
    $scope.types = ['all', 0,1,2,3,4,5, 7, 9,10,11,12, 16, 254,"genesis"];

    $scope.pathname = location.pathname;
    $scope.isloaded = false;

    let url = '/api/v2' + location.pathname;

    if(location.pathname.indexOf('/account/') === -1){
        url = '/api/v2/account' + location.pathname;
    }
    let currentAddress = document.getElementById('currentAddress').innerText;


    $scope.refreshWithGT = function(){

        let search = location.search.replace(/\&isGt10000\=[10]/g, '');
        search = search.replace(/page\=\d/g, '')
        if(search.length == 0) search = '?';

        location.href = location.pathname  + search+'&isGt10000=' + ($scope.isGt10000 ? 1 : 0);
    };

    $http.get(url + location.search).then(function (res) {
        $scope.account = res.data.account;
        $scope.leaseAndCancel = res.data.leaseAndCancel;
        $scope.txns = res.data.txns.map(function(tx){
            if(typeof tx.data == 'string') tx.data = JSON.parse(tx.data);
            tx.isSpent = false;
            if(tx.type == 0 ||  tx.type == 2){
                tx.isSpent = currentAddress == tx.from;
            }
            if(tx.type == 3){
                tx.isSpent = currentAddress == tx.to;
            }
            return tx;
        });
        $scope.page = res.data.page;
        $scope.pager = res.data.pager;
        $scope.page_number = res.data.page_number;
        $scope.isGt10000 = res.data.isGt10000 == 1 ? true : false;
        $scope.isloaded = true;
    });

    $scope.changeUrl = function () {

        location.href = location.pathname + '?page=1&isGt10000=' + ($scope.isGt10000 ? 1 : 0);
    }
}]);
Blacknet.controller('bodyController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    $scope.overview = {};
    
    function toggleNav() {
        let display = document.querySelector('.nav').style.display;

        display = display != 'block' ? 'block': 'none';
        document.querySelector('.nav').style.display = display;
    }

    document.querySelector('.touch-menu button').addEventListener('touchstart', toggleNav ,false);
    $scope.Users = {};
    // $scope.getName = function(address){
    //     console.log($scope.Users[address], $scope.Users)
    //     return $scope.Users[address] || address;
    // };

    $http.get('/api/users').then(function(res){
        let users = res.data;
        users.map(function(user){
            if(user.profile && user.profile.nickname)
            $scope.Users[user.address] = user.profile.nickname;
        })
        
    })
}]);


Blacknet.controller('searchController', ['$scope', '$http', function ($scope, $http) {

    $scope.search = function(){
        if($scope.searchText)
            location.href = '/search?q=' + $scope.searchText;
    };
}]);


Blacknet.controller('aboutController', ['$scope', '$http', function ($scope, $http) {

    $scope.Supply = Supply;

    $http.get('/api/price' + location.search).then(function (res) {
        $scope.price = res.data.price;
        if($scope.price["BLN-BTC"] && parseFloat($scope.price["BLN-BTC"].percent) < 0){
            $scope.percentColor = "red"
        }else{
            $scope.percentColor = "green"
        }
        Highcharts.chart('chart-trade', {
            credits:{
                enabled: false
            },
            chart: {
                zoomType: 'x',
                backgroundColor: "#3B3B3B",
                plotBackgroundColor: "#3B3B3B",
                borderWidth:0
            },
            title: {
                text: '',
                enabled: false
            },
            subtitle: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    millisecond: '%H:%M:%S.%L',
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%m-%d',
                    week: '%m-%d',
                    month: '%Y-%m',
                    year: '%Y'
                },
                visible: false
            },
            tooltip: {
                dateTimeLabelFormats: {
                    millisecond: '%H:%M:%S.%L',
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%Y-%m-%d',
                    week: '%m-%d',
                    month: '%Y-%m',
                    year: '%Y'
                },
                enabled: true,
                pointFormatter: function() {
                    return '<b>'+ this.series.name +'</b>: '+ parseFloat(this.y).toFixed(8) + " BTC";
                }
            },
            yAxis: {
                title: {
                    enabled: false
                },
                visible: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },
            series: [{
                type: 'area',
                name: 'BLN/BTC',
                data: res.data.lines
            }]
        });
    });
}]);

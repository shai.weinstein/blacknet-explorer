module.exports = {

    verifyAccount: (account)=>{
        account = String(account)
        if(account.length > 21 && /^blacknet[a-z0-9]{59}$/i.test(account)){
            return true
        }
        return false
    },
    verifyHash: (hash)=>{
        if(/(\w){64}/i.test(String(hash))){
            return true
        }
        return false
    },
    verifySign: (sign)=>{
        if(Object.prototype.toString.call(sign) === "[object String]" && sign.length === 128){
            return true
        }
        return false
    },
    verifyAlias: (alias)=>{
        if(Object.prototype.toString.call(alias) === "[object String]"){
            return true
        }
        return false
    },
    verifyBlockNumber: (height)=>{
        if(/^\d*$/i.test(height)){
            return true
        }
        return false
    }

}
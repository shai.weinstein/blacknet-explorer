

const {Base64} = require('js-base64');
async function processMessage(tx){

    if(!tx.data || !tx.data.message  || tx.data.message.length < 3) return;


    let text = tx.data.message, data = {};

    let isExistsMessage = await Message.findOne({txid: tx.txid});
    if(isExistsMessage){
        console.log(`${tx.txid} has been exist`);
        return;
    } 

    let message = {
        text: text,
        from: tx.from,
        to: tx.to,
        time: tx.time,
        txid: tx.txid,
        amount: tx.amount,
        msgOwner: [tx.from, tx.to],
        data: {}
    }
    
    if(text.indexOf(':') !== -1){
        
        message.type = text.split(':')[0];

        try{
            data = JSON.parse(text.split(': ')[1]);
        }catch(e){

        }
    }

    if(text.indexOf('createGroup: ') === 0){
        try {
            await updateChatMapWithGroups(tx.txid, tx.from);
        }catch(e){
            console.log(`create group type Error:\n` + e.stack)
        }
    }
    if(text.indexOf('post: ') === 0){
        try {
            data = JSON.parse(Base64.decode(text.slice(6)));
            if(data.replyid && !data.quote ){

                let parentMsg = await Message.findOne({txid: data.replyid});
                if(parentMsg){
                    let tmp = Object.create(parentMsg.data);

                    tmp.lastreply = tx.time;
                    parentMsg.data = tmp;

                    await parentMsg.save();
                }
            }else{
                data.lastreply = tx.time;
            }

            if(data.quote){
                let parentMsg = await Message.findOne({txid: data.quote});
                if(parentMsg){
                    let tmp = Object.create(parentMsg.data);

                    tmp.hasComment = true;
                    parentMsg.data = tmp;

                    await parentMsg.save();
                }
            }

        } catch (error) {
            console.error(error)
        }
    }
    const developFundAddress = 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e';

    if(text.indexOf('chat: ') === 0 && tx.amount != 1000000){
        try {
            let privateMessage = text;
            await updateChatMapWithContacts(tx.time, tx.from, tx.to, privateMessage);
            await updateChatMapWithContacts(tx.time, tx.to, tx.from, privateMessage);

        } catch (error) {
            console.error(error)
        }
    }

    if(message.type == 'addPublicGroup'){
        try {
            let txid = message.text.replace('addPublicGroup: ', '')
            await updateChatMapWithGroups(txid, tx.from);

        } catch (error) {
            console.log(`addPublicGroup type Error:\n` + e.stack)
        }
    }

    if(text.indexOf('profile: ') === 0){
        try {
            let address = message.from;
            let data = JSON.parse(Base64.decode(text.slice(9)));
            
            let account = await Account.findOne({ address });
            if (account) {
              if (!account.profile) account.profile = {};
      
              let tmp = Object.create(account.profile);
      
              for (var key in data) {
                  tmp[key] = data[key];
              }
              account.profile = tmp;
              await account.save(function(error){
                  console.log(error)
              });
            }
        } catch (error) {
            console.error(error)
        }
    }

    message.data = data;
    msg = new Message(message);
    await msg.save();

}


async function updateChatMapWithContacts(time, address, withAddress, message){

    let chatMap = await ChatMap.findOne({address: address});
    
    if(!chatMap){
        let contacts = {}
        contacts[withAddress] = {
            time: time,
            message: message
        }

        chatMap = new ChatMap({
            address: address,
            contacts,
            groups: {}
        });
    }else{

        let tmp = Object.create(chatMap.contacts);
        tmp[withAddress] = {
            time: time,
            message: message
        }
        chatMap.contacts = tmp;
    }
    await chatMap.save();
}

async function updateChatMapWithGroups(txid, address){

    
    let chatMap = await ChatMap.findOne({address});
    if(!chatMap){

        let groups = {}
        groups[txid] = {}

        chatMap = new ChatMap({
            address: address,
            groups
        });
    }else{
        let tmp = Object.create(chatMap.groups || {});
        tmp[txid] = {}
        chatMap.groups = tmp;
    }
    await chatMap.save();
}

module.exports = {

    processMessage
}
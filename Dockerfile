FROM node:13

# args
ARG API_ENV
ENV API_ENV=$API_ENV
ARG APP_VERSION
ENV APP_VERSION=$APP_VERSION

# ENV 
ENV BLACKNET_URL 'http://127.0.0.1:8283'
ENV MONGODB_URL 'mongodb://localhost/test'

WORKDIR /web
ADD . ./

RUN npm install
RUN npm install pm2 -g

EXPOSE 3000

CMD pm2 start init.js && pm2 start app.js --no-daemon